/// @description Insert description here
// You can write your code in this editor

if (instance_exists(ob_player))
{
	if (flag_attack == false)
	{
		var distance = distance_to_object(ob_player)
	
		if (distance > distance_to_attack)
		{
			angle = point_direction(x,y,ob_player.x, ob_player.y)
			mp_potential_path(path_ref, ob_player.x, ob_player.y, 3, 4, 0);
			path_start(path_ref, move_speed, 0, 0);
		}
		else
		{
			alarm[0] = 60
			flag_attack = true
		}
	}
	else
	{
		path_end()
		angle = point_direction(x,y,ob_player.x, ob_player.y)
		speed = 0
	}
}
else
{
	speed = move_speed/2
	
	if (place_meeting(x+hspeed, y, ob_wall)) {
	    // Inverti la direzione dell'oggetto rimbalzante lungo l'asse x
	    hspeed = -hspeed;
		flag_coll = true
	}

	if (place_meeting(x, y+vspeed, ob_wall)) {
	    // Inverti la direzione dell'oggetto rimbalzante lungo l'asse x
	    vspeed = -vspeed;
		flag_coll = true
	}
}






