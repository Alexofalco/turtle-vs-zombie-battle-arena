/// @description Insert description here
// You can write your code in this editor

var dx = 0
var dy = 0

var _speed = flag_guscio ? move_speed : move_speed_noguscio

if (keyboard_check(vk_up))
{
	dy-=_speed
	angle = point_direction(x,y,x+dx, y+dy);
}
if (keyboard_check(vk_down))
{
	dy+=_speed
	angle = point_direction(x,y,x+dx, y+dy);
}

if (keyboard_check(vk_left))
{
	dx-=_speed
	angle = point_direction(x,y,x+dx, y+dy);
}
if (keyboard_check(vk_right))
{
	dx+=_speed
	angle = point_direction(x,y,x+dx, y+dy);
}


flag_move = dx != 0 or dy != 0

if (flag_guscio)
{
	if (flag_move)
	{
		sprite_index = spr_player
		image_speed = 1
	}
	else
	{
		sprite_index = spr_player
		image_speed = 0
	}

	if (keyboard_check_pressed(vk_space))
	{		
		var _ob = instance_create_depth(x,y,depth,ob_bullet)
		_ob.speed = move_speed_bullet
		_ob.direction = point_direction(x,y,x+dx, y+dy);
		flag_guscio = false
	}
}
else
{
	if (flag_move)
	{
		sprite_index = spr_player_noguscio
		image_speed = 1
	}
	else
	{
		sprite_index = spr_player_noguscio_idle
		image_speed = 0
	}
	
	//if (distance_to_object(ob_bullet) < 50)
	//{
	//	if (ob_bullet.flag_coll = true)
	//	{
	//		instance_destroy(ob_bullet)
	//		flag_guscio = true
	//	}
	//}
}


move_and_collide(dx, dy, ob_wall)






