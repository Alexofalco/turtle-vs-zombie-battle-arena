/// @description Insert description here
// You can write your code in this editor

switch(state)
{
	case GAME_STATUS_ENUM.start:
	case GAME_STATUS_ENUM.finish:
	
		if (audio_is_playing(sfx_music_menu) == false)
		{
			audio_play_sound(sfx_music_menu, 10, true)
		}
		instance_destroy(ob_actor)
		if (keyboard_check_pressed(vk_enter))
		{
			state = GAME_STATUS_ENUM.game_loop;
			instance_create_depth(room_width/2, room_height/2, depth, ob_player)
			point = 0
			time = 0
			level = 1
			audio_stop_all()
		}
		if (keyboard_check_pressed(vk_escape))
		{
			game_end()
		}
		break;
	case GAME_STATUS_ENUM.game_loop:
		if (audio_is_playing(sfx_music_game_loop) == false)
		{
			audio_play_sound(sfx_music_game_loop, 10, true)
		}
		
		if (instance_exists(ob_player) == false)
		{
			state = GAME_STATUS_ENUM.finish
			audio_stop_all()
			if (audio_is_playing(sfx_music_menu) == false)
			{
				audio_play_sound(sfx_music_menu, 10, true)
			}
		}
		
		time +=1
		time_max = 60 + 60 * 30 * (level)

		if (time > time_max)
		{
			time = 0
			level += 1
		}

		var curve_diff_enemy1 = max(90 - level* 5, 30)
		var curve_diff_enemy2 = max(180 - level* 5, 60)

		if (time mod curve_diff_enemy1 == 0)
		{
			if (instance_number(ob_enemy1) < 30)
			{
				if (instance_exists(ob_player))
				{
					var _point = get_enemy_spawn_point(100)
					if (_point != noone)
					{
						var _ob = instance_create_layer(_point.x, _point.y, depth, ob_enemy1)
					}
				}
			}
		}
		if (time mod curve_diff_enemy2 == 0)
		{
			if (instance_number(ob_enemy2) < 6)
			{
				if (instance_exists(ob_player))
				{
					var _point = get_enemy_spawn_point(300)
					if (_point != noone)
					{
						var _ob = instance_create_layer(_point.x, _point.y, depth, ob_enemy2)
					}
				}
			}
		}
		break;

	
}







