/// @description Insert description here
// You can write your code in this editor

switch(state)
{
	case GAME_STATUS_ENUM.game_loop:
		draw_set_color(c_white)
		//draw_text(10,10,"time: "+string(time)+"/"+string(time_max) + " : "+string(level))

		draw_set_halign(fa_left)
		draw_set_valign(fa_top)
		draw_set_color(#634B63)
		draw_rectangle(10,10,room_width-10, 10+30, 0)

		draw_set_color(#A349A4)
		var value = time/time_max * (room_width-20)
		draw_rectangle(10,10,10+value, 10+30, 0)

		draw_set_color(#ECBBED)
		draw_text(20,15, "level: "+string(level))

		draw_set_color(#634B63)
		draw_rectangle(10,45,200, 45+30, 0)
		
		draw_set_color(#ECBBED)
		draw_text(20,50, "point: "+string(floor(point)))
		break;
		
	case GAME_STATUS_ENUM.start:
		draw_sprite(spr_title, 0, room_width/2, room_height/2-100)
		
		draw_set_halign(fa_center)
		draw_set_valign(fa_center)
		
		draw_set_color(#634b63)
		draw_text(room_width/2, room_height/2+100, "WASD per muoversi\nSpazio per lanciare il guscio")
		draw_text(room_width/2, room_height/2+200, "Un gioco realizzato per Scario da XXX e YYY per la Secret Santa Jam 23")
		draw_text(room_width/2, room_height/2 + 300, "Premi Invio per ricominciare")
		break;
		
	case GAME_STATUS_ENUM.finish:
		draw_sprite(spr_lose, 0, room_width/2, room_height/2)
		
		draw_set_halign(fa_center)
		draw_set_valign(fa_center)
		
		draw_set_color(#634b63)
		draw_text(room_width/2, room_height/2+100, "Punteggio: " + string(point))
		draw_text(room_width/2, room_height/2+200, "Premi Invio per ricominciare")
		break;
}






