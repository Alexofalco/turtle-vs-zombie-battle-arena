{
  "resourceType": "GMSprite",
  "resourceVersion": "1.0",
  "name": "spr_enemy2",
  "bbox_bottom": 27,
  "bbox_left": 5,
  "bbox_right": 25,
  "bbox_top": 5,
  "bboxMode": 2,
  "collisionKind": 1,
  "collisionTolerance": 0,
  "DynamicTexturePage": false,
  "edgeFiltering": false,
  "For3D": false,
  "frames": [
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"1d6532c8-b5d8-4627-ad5a-bd295020f5f3",},
    {"resourceType":"GMSpriteFrame","resourceVersion":"1.1","name":"180ff24b-b048-4239-aeb8-193d98e188dd",},
  ],
  "gridX": 0,
  "gridY": 0,
  "height": 34,
  "HTile": false,
  "layers": [
    {"resourceType":"GMImageLayer","resourceVersion":"1.0","name":"b9c2b910-5c1e-4b15-8ff3-67ad3efea72d","blendMode":0,"displayName":"default","isLocked":false,"opacity":100.0,"visible":true,},
  ],
  "nineSlice": null,
  "origin": 4,
  "parent": {
    "name": "Actors",
    "path": "folders/Actors.yy",
  },
  "preMultiplyAlpha": false,
  "sequence": {
    "resourceType": "GMSequence",
    "resourceVersion": "1.4",
    "name": "spr_enemy2",
    "autoRecord": true,
    "backdropHeight": 768,
    "backdropImageOpacity": 0.5,
    "backdropImagePath": "",
    "backdropWidth": 1366,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "events": {"resourceType":"KeyframeStore<MessageEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "eventStubScript": null,
    "eventToFunction": {},
    "length": 2.0,
    "lockOrigin": false,
    "moments": {"resourceType":"KeyframeStore<MomentsEventKeyframe>","resourceVersion":"1.0","Keyframes":[],},
    "playback": 1,
    "playbackSpeed": 8.0,
    "playbackSpeedType": 0,
    "showBackdrop": true,
    "showBackdropImage": false,
    "timeUnits": 1,
    "tracks": [
      {"resourceType":"GMSpriteFramesTrack","resourceVersion":"1.0","name":"frames","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"1.0","Keyframes":[
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"1d6532c8-b5d8-4627-ad5a-bd295020f5f3","path":"sprites/spr_enemy2/spr_enemy2.yy",},},},"Disabled":false,"id":"c736905b-04d0-4436-8cd5-0f3608b9f55d","IsCreationKey":false,"Key":0.0,"Length":1.0,"Stretch":false,},
            {"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"1.0","Channels":{"0":{"resourceType":"SpriteFrameKeyframe","resourceVersion":"1.0","Id":{"name":"180ff24b-b048-4239-aeb8-193d98e188dd","path":"sprites/spr_enemy2/spr_enemy2.yy",},},},"Disabled":false,"id":"1606d586-c118-4643-aa61-19292844df76","IsCreationKey":false,"Key":1.0,"Length":1.0,"Stretch":false,},
          ],},"modifiers":[],"spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange": null,
    "volume": 1.0,
    "xorigin": 15,
    "yorigin": 17,
  },
  "swatchColours": null,
  "swfPrecision": 2.525,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "type": 0,
  "VTile": false,
  "width": 31,
}