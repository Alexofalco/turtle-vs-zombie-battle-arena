// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function get_enemy_spawn_point(distance){
	if (instance_exists(ob_player))
	{
		var cond = 0
		var xrandom = noone
		var yrandom = noone
		while(cond < 10)
		{
			xrandom = 60 + irandom(room_width-120)
			yrandom = 126 + irandom(room_height-180)
	
			if (point_distance(ob_player.x, ob_player.y, xrandom, yrandom) > distance)
			{
				cond = 99
			}
			else
			{
				cond += 1
			}
		}
	}
	
	if (xrandom == noone or yrandom == noone)
	{
		return noone;
	}
	else
	{
		return 
		{
			x: xrandom,
			y: yrandom
		}
	}
}